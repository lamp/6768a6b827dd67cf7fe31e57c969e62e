global.G = global;
G.Discord = require("discord.io");
G.fs = require("fs");

G.serverID = "321819041348190249";
G.channelID = "600091812450795523";

G.tokens = fs.readFileSync("tokens.txt", "utf8").split("\n");

G.clients = tokens.map((token, index) => {
    var client = new Discord.Client({
        token, autorun: true
    });
    client.on("error", error => console.error(index, error));
    client.on("connect", () => console.log(index, "connect"));
    client.on("disconnect", () => console.log(index, "disconnect"));
    client.on("ready", () => {
        console.log(index, "ready");
        client.joinVoiceChannel(channelID, function(error) {
            if (error) console.error(error);
            console.log(index, "connected");
            client.getAudioContext(channelID, function(error, ctx){
                if (error) console.error("ctx", index, error);
                console.log(index, "ctx ready");
                client.ctx = ctx;
                client.startSpeaking = function() { this.ctx._voiceSession.ws.connection.send(`{"op":5,"d":{"speaking":true,"delay":0}}`); };
                client.stopSpeaking = function() { this.ctx._voiceSession.ws.connection.send(`{"op":5,"d":{"speaking":false,"delay":0}}`); };
            });
        });
    });
    /*setTimeout(()=>{
        client.connect();
    }, index*1000);*/
    return client;
});


/*(function connectNextClient(i){
    var client = clients[i];
    if (!client) return;
    client.connect();
    client.once("ready", () => {
        connectNextClient(++i);
    });
})(0);*/


G.JZZ = require("jzz");
G.midiOut = JZZ().or('Cannot start MIDI engine!').openMidiOut().or('Cannot open MIDI Out port!');


G.MidiPlayer = require("midi-player-js");
G.player = new MidiPlayer.Player(async function(event){
    if (event.noteNumber) {
        var index = event.noteNumber - 21
        var client = clients[index];
        if (!client) return console.error("no client at", index);
    }
    if (event.name == "Note off" || (event.name == "Note on" && event.velocity === 0)) {
        midiOut.send([0x80,event.noteNumber,0]);
        client.stopSpeaking();
    } else if (event.name == "Note on") {
        midiOut.send([0x90,event.noteNumber,127]);
        client.startSpeaking();
    } else if (event.name == "Set Tempo") {
        //player.setTempo(event.data);
    }
});

setInterval(()=>{}, 10000);